import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { Thermo2Component } from './components/thermo2/thermo2.component';
import { HttpClientModule } from '@angular/common/http';
import { DataService } from './data.service';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { MeteoComponent } from './components/meteo/meteo.component';
import { ComponentDetailsComponent } from './components/component-details/component-details.component';
import { CartesdetailComponent } from './components/cartesdetail/cartesdetail.component';
import { ListeDomotiquesComponent } from './pages/liste-domotiques/liste-domotiques.component';
import { ComponentsPlanComponent } from './components/components-plan/components-plan.component';
import { DetailLumiereComponent } from './pages/detail-lumiere/detail-lumiere.component';
import { DetailAppareilsComponent } from './pages/detail-appareils/detail-appareils.component';
import { DetailStoreComponent } from './pages/detail-store/detail-store.component';
import { DetailThermoComponent } from './pages/detail-thermo/detail-thermo.component';
import {TestchartjsComponent } from './components/testchartjs/testchartjs.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    Thermo2Component,
    HomePageComponent,
    MeteoComponent,
    ComponentDetailsComponent,
    CartesdetailComponent,
    ListeDomotiquesComponent,
    ComponentsPlanComponent,
    DetailLumiereComponent,
    DetailAppareilsComponent,
    DetailStoreComponent,
    DetailThermoComponent,
    TestchartjsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }