import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestchartjsComponent } from './testchartjs.component';

describe('TestchartjsComponent', () => {
  let component: TestchartjsComponent;
  let fixture: ComponentFixture<TestchartjsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestchartjsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestchartjsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
