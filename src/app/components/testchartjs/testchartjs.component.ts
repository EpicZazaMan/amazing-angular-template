import { Component, OnInit } from '@angular/core';
import { Thermo } from '../../models/thermo.model' ;
import { DataService } from 'src/app/data.service';

@Component({
  selector: 'app-testchartjs',
  templateUrl: './testchartjs.component.html',
  styleUrls: ['./testchartjs.component.scss']
})
export class TestchartjsComponent implements OnInit {

  thermE: Thermo[];
  thermI: Thermo[];
  constructor(private dataService: DataService) { }


  ngOnInit() {
    this.dataService.getThermoExt().subscribe(data => this.thermE = data);
    this.dataService.getThermoInt().subscribe(data => this.thermI = data);
  }
}