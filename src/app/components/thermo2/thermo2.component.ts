import { Component, OnInit } from '@angular/core';
import { Thermo } from '../../models/thermo.model' ;
import { DataService } from 'src/app/data.service';
@Component({
  selector: 'app-thermo2',
  templateUrl: './thermo2.component.html',
  styleUrls: ['./thermo2.component.scss']
})
export class Thermo2Component implements OnInit {

  thermE: Thermo[];
  thermI: Thermo[];
  constructor(private dataService: DataService) { }


  ngOnInit() {
    this.dataService.getLastThermoExt().subscribe(data => this.thermE = data);
    this.dataService.getLastThermoInt().subscribe(data => this.thermI = data);
  }
}

// export class Thermo1Component implements OnInit {

//   thermI: ThermoINT[];
//   constructor(private dataService: DataService) { }

//   ngOnInit() {
//     return this.dataService.getLastThermoInt().subscribe(data => this.thermI = data);
//   }
// }
