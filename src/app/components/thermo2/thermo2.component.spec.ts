import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Thermo2Component } from './thermo2.component';

describe('Thermo2Component', () => {
  let component: Thermo2Component;
  let fixture: ComponentFixture<Thermo2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Thermo2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Thermo2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
