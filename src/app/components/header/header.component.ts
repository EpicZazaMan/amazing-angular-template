import { Component, OnInit } from '@angular/core';
import { User } from '../../models/user.model';
import { DataService } from 'src/app/data.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  users$: User[];
  constructor(private dataService: DataService) { }

  ngOnInit() {
    // return this.dataService.getUsers().subscribe(data => this.users$ = data);
  }

}
