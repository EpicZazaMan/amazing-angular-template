import { Component, OnInit,Input  } from '@angular/core';

@Component({
  selector: 'app-cartesdetail',
  templateUrl: './cartesdetail.component.html',
  styleUrls: ['./cartesdetail.component.scss']
})
export class CartesdetailComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  @Input() icon: string;
  @Input() name: string;

}
