import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CartesdetailComponent } from './cartesdetail.component';

describe('CartesdetailComponent', () => {
  let component: CartesdetailComponent;
  let fixture: ComponentFixture<CartesdetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CartesdetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CartesdetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
