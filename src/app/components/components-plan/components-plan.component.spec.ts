import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponentsPlanComponent } from './components-plan.component';

describe('ComponentsPlanComponent', () => {
  let component: ComponentsPlanComponent;
  let fixture: ComponentFixture<ComponentsPlanComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComponentsPlanComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComponentsPlanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
