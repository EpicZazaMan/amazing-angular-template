import { Component, OnInit } from '@angular/core';
import { Domotique } from '../../models/domo.model';
import { DataService } from 'src/app/data.service';
@Component({
  selector: 'app-components-plan',
  templateUrl: './components-plan.component.html',
  styleUrls: ['./components-plan.component.scss']
})
export class ComponentsPlanComponent implements OnInit {

  domo: Domotique[];
  obj:any;
  constructor(private dataService: DataService) { }

  ngOnInit() {
    return this.dataService.getDOMOPLUS().subscribe(data => this.domo = data);
  }

  switchStatFalse(id) {
    this.dataService.getDOMOPLUSById(id).subscribe(() => {
      this.obj = { "etat": false }
      console.log(this.obj);
      this.dataService.putDOMOPLUSById(id, this.obj).subscribe(data => {
        console.log(data);
        document.location.reload();
      }, error => console.log(error));
    });
    
  }
  switchStatTrue(id) {
    this.dataService.getDOMOPLUSById(id).subscribe(() => {
      this.obj = { "etat": true }
      console.log(this.obj);
      this.dataService.putDOMOPLUSById(id, this.obj).subscribe(data => {
        console.log(data);
        document.location.reload();
      }, error => console.log(error));
    });
    
  }

}
