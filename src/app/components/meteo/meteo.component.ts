import { Component, OnInit } from '@angular/core';
import { Meteo } from '../../models/meteo.model';
import { DataService } from 'src/app/data.service';
import { element } from '@angular/core/src/render3';

@Component({
  selector: 'app-meteo',
  templateUrl: './meteo.component.html',
  styleUrls: ['./meteo.component.scss']
})
export class MeteoComponent implements OnInit {

  meteo: Meteo[] = [new Meteo()];
  temps:string;
  now = new Date();
  hours : any;
  minute : any;
  icon: string;

  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.dataService.getmeteo().subscribe(data => { this.meteo = data; });

    this.hours = this.now.getHours();
    this.minute = this.now.getMinutes();

    if(this.minute < 10){
    this.minute = this.minute.toString();
    this.minute = "0" + this.minute;
    }

    this.temps="clouds";
  

    if (this.temps == "clear sky") {
      this.icon = "sun.svg" 
    } else if(this.temps == "clouds" ){
      this.icon = "cloudy.svg"
    }
    else if(this.temps == "rain") {
      this.icon = "rain.svg"
    }
    else if(this.temps == "thunderstorm") {
      this.icon = "storm.svg"
    }else{
      this.icon = "sun.svg"
    }
  }

}