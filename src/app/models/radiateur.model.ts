export class Radiateur {
    _id: Number;
    ID_machine: Number;
    conso: Number;
    temperature: Number;
    date: Date;
    __v: Number;
}
