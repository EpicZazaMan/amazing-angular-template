import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Domotique } from './models/domo.model';
import { Thermo } from './models/thermo.model';
import { Lumiere } from './models/lumiere.model';
import { Refrigerateur } from './models/refrigerateur.model';
import { Radiateur } from './models/radiateur.model';
import { LaveLinge } from './models/lavelinge.model';
import { Store } from './models/store.model';
import {Meteo} from './models/meteo.model';
import { CATCH_ERROR_VAR } from '@angular/compiler/src/output/output_ast';

@Injectable({
  providedIn: 'root'
})
export class DataService {

 

  constructor(private _http: HttpClient) { }


  getDOMOPLUS() {
    var apiUrl = 'https://api-domoplus.herokuapp.com/domotique';
    return this._http.get<Domotique[]>(apiUrl);
  }
  getDOMOPLUSById(id) {
    var apiUrl = 'https://api-domoplus.herokuapp.com/domotique/'+ id;
    return this._http.get<Domotique>(apiUrl);
  }
  putDOMOPLUSById(id,body) {
    var apiUrl = 'https://api-domoplus.herokuapp.com/domotique/'+ id;
    console.log(apiUrl);
    return this._http.put<void>(apiUrl,body,{
      headers: new HttpHeaders({
        'Content-Type':'application/json'
      })
    });
    
  }
  getLastThermoExt() {
    var apiUrl = 'https://api-domoplus.herokuapp.com/thermo/EXTlast';
    return this._http.get<Thermo[]>(apiUrl);
  }
  getLastThermoInt() {
    var apiUrl = 'https://api-domoplus.herokuapp.com/thermo/INTlast';
    return this._http.get<Thermo[]>(apiUrl);
  }
  getLastlumiere1() {
    var apiUrl = 'https://api-domoplus.herokuapp.com/lumiere1last';
    return this._http.get<Lumiere[]>(apiUrl);
  }
  getLastlumiere2() {
    var apiUrl = 'https://api-domoplus.herokuapp.com/lumiere2last';
    return this._http.get<Lumiere[]>(apiUrl);
  }
  getLastlumiere3() {
    var apiUrl = 'https://api-domoplus.herokuapp.com/lumiere3last';
    return this._http.get<Lumiere[]>(apiUrl);
  }
  getLastRefrigerator() {
    var apiUrl = 'https://api-domoplus.herokuapp.com/refrigerateurlast';
    return this._http.get<Refrigerateur[]>(apiUrl);
  }
  getLastRadiateur() {
    var apiUrl = 'https://api-domoplus.herokuapp.com/radiateurlast';
    return this._http.get<Radiateur[]>(apiUrl);
  }
  getLastLavelinge() {
    var apiUrl = 'https://api-domoplus.herokuapp.com/lavelingelast';
    return this._http.get<LaveLinge[]>(apiUrl);
  }
  getLastStore() {
    var apiUrl = 'https://api-domoplus.herokuapp.com/storelast';
    return this._http.get<Store[]>(apiUrl);
  }
  getThermoExt() {
    var apiUrl = 'https://api-domoplus.herokuapp.com/thermo/EXT';
    return this._http.get<Thermo[]>(apiUrl);
  }
  getThermoInt() {
    var apiUrl = 'https://api-domoplus.herokuapp.com/thermo/INT';
    return this._http.get<Thermo[]>(apiUrl);
  }
  getmeteo(){
    var apiUrl = 'http://api.openweathermap.org/data/2.5/weather?q=Nantes&units=metric&appid=03be4c5a61638dff2e699169f9657c52';
    return this._http.get<any[]>(apiUrl);
  }
}
