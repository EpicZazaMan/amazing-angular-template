import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HeaderComponent } from './components/header/header.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { ListeDomotiquesComponent } from './pages/liste-domotiques/liste-domotiques.component';
import { DetailAppareilsComponent } from './pages/detail-appareils/detail-appareils.component';
import { DetailLumiereComponent } from './pages/detail-lumiere/detail-lumiere.component';
import { DetailStoreComponent } from './pages/detail-store/detail-store.component';
import { TestchartjsComponent } from './components/testchartjs/testchartjs.component';
import { DetailThermoComponent } from './pages/detail-thermo/detail-thermo.component';

const routes: Routes = [
  {
    path: '',
    component: HomePageComponent
  },
  {
    path: 'header',
    component: HeaderComponent
  },
  {
    path: 'home',
    component: HomePageComponent
  },
  {
    path: 'listdomo',
    component: ListeDomotiquesComponent
  },
  {
    path: 'listdomo/lumiere1',
    component: DetailLumiereComponent,
    data: { ID_machine: 1 ,nom:'Lumiere 1'}
  },
  {
    path: 'listdomo/lumiere2',
    component: DetailLumiereComponent,
    data: { ID_machine: 2,nom:'Lumiere 2' }
  },
  {
    path: 'listdomo/lumiere3',
    component: DetailLumiereComponent,
    data: { ID_machine: 3 ,nom:'Lumiere 3'}
  },
  {
    path: 'listdomo/frigo',
    component: DetailAppareilsComponent,
    data: { ID_machine:4 ,nom:'Frigo' }
  },
  {
    path: 'listdomo/radiateur',
    component: DetailAppareilsComponent,
    data: { ID_machine: 5,nom:'Radiateur' }
  },
  {
    path: 'listdomo/lavelinge',
    component: DetailAppareilsComponent,
    data: { ID_machine: 6,nom:'Lavelinge' }
  },
  {
    path: 'listdomo/store',
    component: DetailStoreComponent,
    data: {  ID_machine: 7,nom:'Store' }

  },
  {
    path: 'listdomo/thermoEXT',
    component: DetailThermoComponent,
    data: { ID_machine: 8,nom:'Thermomètre EXT' }
  },
  {
    path: 'listdomo/thermoINT',
    component: DetailThermoComponent,
    data: { ID_machine: 9 ,nom:'Thermomètre INT'}
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
