import { Component, OnInit } from '@angular/core';
import { Thermo } from 'src/app/models/thermo.model';
import { DataService } from 'src/app/data.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-detail-thermo',
  templateUrl: './detail-thermo.component.html',
  styleUrls: ['./detail-thermo.component.scss']
})
export class DetailThermoComponent implements OnInit {
  data: any;
  thermometre: Thermo[];
  obj: any ;
  constructor(private route: ActivatedRoute,private dataService: DataService) { }


  ngOnInit() {
    this.route
    .data
    .subscribe(data => this.data = data);

    if (this.data.ID_machine==8){
      this.dataService.getLastThermoExt().subscribe(data => this.thermometre = data);
    }
    else if (this.data.ID_machine==9){
      this.dataService.getLastThermoInt().subscribe(data => this.thermometre = data);
    }
    
    
  }

  switchStatement(){
    
    this.dataService.getDOMOPLUSById(this.data.ID_machine).subscribe(data => {
    if(data.etat==true){
      this.obj= {"etat":false}
      console.log(this.obj);
      this.dataService.putDOMOPLUSById(this.data.ID_machine,this.obj).subscribe(data=>{
        console.log(data);
      },error=>console.log(error));
    }
    else if (data.etat==false){
      this.obj  = {"etat":true}
      console.log(this.obj);
      this.dataService.putDOMOPLUSById(this.data.ID_machine,this.obj).subscribe(data=>{
        console.log(data);
      },error=>console.log(error));
    }
      
    });
    
   
    
  }

}
