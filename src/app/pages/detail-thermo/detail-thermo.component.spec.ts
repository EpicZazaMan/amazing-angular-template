import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailThermoComponent } from './detail-thermo.component';

describe('DetailThermoComponent', () => {
  let component: DetailThermoComponent;
  let fixture: ComponentFixture<DetailThermoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailThermoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailThermoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
