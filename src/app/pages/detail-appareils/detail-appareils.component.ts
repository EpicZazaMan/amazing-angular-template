import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from 'src/app/data.service';
import { Radiateur } from 'src/app/models/radiateur.model';
import { Refrigerateur } from 'src/app/models/refrigerateur.model';
import { LaveLinge } from 'src/app/models/lavelinge.model';

@Component({
  selector: 'app-detail-appareils',
  templateUrl: './detail-appareils.component.html',
  styleUrls: ['./detail-appareils.component.scss']
})
export class DetailAppareilsComponent implements OnInit {

  data: any;
  appareil: any[];
  obj: any ;
  constructor(private route: ActivatedRoute,private dataService: DataService) {}

  ngOnInit() {
   this.route
    .data
    .subscribe(data => this.data = data);

    if (this.data.ID_machine==5){
      this.dataService.getLastRadiateur().subscribe(data => {this.appareil = data;console.log(data)});
    }
    else if (this.data.ID_machine==6){
      this.dataService.getLastLavelinge().subscribe(data => this.appareil = data);
    }
    else if (this.data.ID_machine==4){
      this.dataService.getLastRefrigerator().subscribe(data => this.appareil = data);
    }
  };


  
  switchStatement(){
    
    this.dataService.getDOMOPLUSById(this.data.ID_machine).subscribe(data => {
    if(data.etat==true){
      this.obj= {"etat":false}
      console.log(this.obj);
      this.dataService.putDOMOPLUSById(this.data.ID_machine,this.obj).subscribe(data=>{
        console.log(data);
      },error=>console.log(error));
    }
    else if (data.etat==false){
      this.obj  = {"etat":true}
      console.log(this.obj);
      this.dataService.putDOMOPLUSById(this.data.ID_machine,this.obj).subscribe(data=>{
        console.log(data);
      },error=>console.log(error));
    }
    }); 
  }
}
