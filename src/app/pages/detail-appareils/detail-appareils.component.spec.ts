import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailAppareilsComponent } from './detail-appareils.component';

describe('DetailAppareilsComponent', () => {
  let component: DetailAppareilsComponent;
  let fixture: ComponentFixture<DetailAppareilsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailAppareilsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailAppareilsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
