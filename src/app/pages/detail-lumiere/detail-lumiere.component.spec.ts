import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailLumiereComponent } from './detail-lumiere.component';

describe('DetailLumiereComponent', () => {
  let component: DetailLumiereComponent;
  let fixture: ComponentFixture<DetailLumiereComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailLumiereComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailLumiereComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
