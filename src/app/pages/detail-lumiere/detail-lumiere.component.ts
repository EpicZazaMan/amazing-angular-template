import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from 'src/app/data.service';
import { Lumiere } from 'src/app/models/lumiere.model';

@Component({
  selector: 'app-detail-lumiere',
  templateUrl: './detail-lumiere.component.html',
  styleUrls: ['./detail-lumiere.component.scss']
})
export class DetailLumiereComponent implements OnInit {
  data: any;
  lumiere: Lumiere[];
  obj: any ;
  constructor(private route: ActivatedRoute,private dataService: DataService) {}

  ngOnInit() {
   this.route
    .data
    .subscribe(data => this.data = data);

    if (this.data.ID_machine==1){
      this.dataService.getLastlumiere1().subscribe(data => {this.lumiere = data;console.log(data)});
    }
    else if (this.data.ID_machine==2){
      this.dataService.getLastlumiere2().subscribe(data => this.lumiere = data);
    }
    else if (this.data.ID_machine==3){
      this.dataService.getLastlumiere3().subscribe(data => this.lumiere = data);
    }
  };
  
  
  switchStatement(){
    
    this.dataService.getDOMOPLUSById(this.data.ID_machine).subscribe(data => {
    if(data.etat==true){
      this.obj= {"etat":false}
      console.log(this.obj);
      this.dataService.putDOMOPLUSById(this.data.ID_machine,this.obj).subscribe(data=>{
        console.log(data);
      },error=>console.log(error));
    }
    else if (data.etat==false){
      this.obj  = {"etat":true}
      console.log(this.obj);
      this.dataService.putDOMOPLUSById(this.data.ID_machine,this.obj).subscribe(data=>{
        console.log(data);
      },error=>console.log(error));
    }
      
    });
    
   
    
  }
  

}
