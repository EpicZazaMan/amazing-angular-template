import { Component, OnInit } from '@angular/core';
import { Store } from 'src/app/models/store.model';
import { ActivatedRoute } from '@angular/router';
import { DataService } from 'src/app/data.service';
import { Domotique } from 'src/app/models/domo.model';

@Component({
  selector: 'app-detail-store',
  templateUrl: './detail-store.component.html',
  styleUrls: ['./detail-store.component.scss']
})
export class DetailStoreComponent implements OnInit {

  data: any;
  store: Store[];
  obj: any ;
  constructor(private route: ActivatedRoute,private dataService: DataService) {}

  ngOnInit() {
    this.route
    .data
    .subscribe(data => this.data = data);

    this.dataService.getLastStore().subscribe(data => this.store = data);
    

  };

  switchStatement(){
    
    this.dataService.getDOMOPLUSById(this.data.ID_machine).subscribe(data => {
    if(data.etat==true){
      this.obj= {"etat":false}
      console.log(this.obj);
      this.dataService.putDOMOPLUSById(this.data.ID_machine,this.obj).subscribe(data=>{
        console.log(data);
      },error=>console.log(error));
    }
    else if (data.etat==false){
      this.obj  = {"etat":true}
      console.log(this.obj);
      this.dataService.putDOMOPLUSById(this.data.ID_machine,this.obj).subscribe(data=>{
        console.log(data);
      },error=>console.log(error));
    }
      
    });
    
   
    
  }
}
