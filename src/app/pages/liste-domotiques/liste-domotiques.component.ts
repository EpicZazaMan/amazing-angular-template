import { Component, OnInit } from '@angular/core';
import { Domotique } from '../../models/domo.model' ;
import { DataService } from 'src/app/data.service';
@Component({
  selector: 'app-liste-domotiques',
  templateUrl: './liste-domotiques.component.html',
  styleUrls: ['./liste-domotiques.component.scss']
})
export class ListeDomotiquesComponent implements OnInit {

  domo$: Domotique[];
  constructor(private dataService: DataService) { }

  ngOnInit() {
    return this.dataService.getDOMOPLUS().subscribe(data => this.domo$ = data);
  }

}
