import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeDomotiquesComponent } from './liste-domotiques.component';

describe('ListeDomotiquesComponent', () => {
  let component: ListeDomotiquesComponent;
  let fixture: ComponentFixture<ListeDomotiquesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListeDomotiquesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeDomotiquesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
